package com.mybatis.plus.example.mapper;

import com.mybatis.plus.example.entity.User;
import com.mybatis.plus.join.mapper.ParentMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends ParentMapper<User> {
}
