package com.mybatis.plus.example.dto;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Map;

/**
 * @author by zhaojin
 * @since 2021/7/5 9:38
 */
@Data
public class UserDTO {

    private String id;
    private String name;
    private String examId;
    private String score;
    private Timestamp createTime;

    @TableField(typeHandler = FastjsonTypeHandler.class)
    private Map<String, Object> extra;
}
