package com.mybatis.plus.example.config;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.MybatisMapWrapperFactory;
import com.mybatis.plus.join.JoinSqlInjector;
import com.mybatis.plus.join.config.DynamicResultMapInterceptor;
import com.mybatis.plus.join.config.MybatisPlusConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MybatisPlusJoinConfig {

    @Bean
    public DynamicResultMapInterceptor dynamicResultMapInterceptor() {
        return new DynamicResultMapInterceptor();
    }

    @Bean
    public JoinSqlInjector joinSqlInjector() {
        return new JoinSqlInjector();
    }

    @Bean
    public ConfigurationCustomizer mybatisConfigurationCustomizer() {
        return (configuration) -> {
            configuration.setObjectWrapperFactory(new MybatisMapWrapperFactory());
        };
    }

}
