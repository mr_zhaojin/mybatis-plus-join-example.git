package com.mybatis.plus.example.service;

import com.mybatis.plus.example.entity.User;
import com.mybatis.plus.join.service.IBaseService;

public interface IUserService extends IBaseService<User> {
}
