package com.mybatis.plus.example.service.impl;

import com.mybatis.plus.example.entity.Score;
import com.mybatis.plus.example.entity.User;
import com.mybatis.plus.join.ConditionEnum;
import com.mybatis.plus.join.service.impl.BaseService;
import com.mybatis.plus.example.mapper.UserMapper;
import com.mybatis.plus.example.service.IUserService;
import org.springframework.stereotype.Service;


@Service
public class UserService extends BaseService<UserMapper, User> implements IUserService {

    public void main() {
        User score = query(User.class).select(User::getName)
                .innerJoin(Score.class)
                .on(Score::getUserId, ConditionEnum.EQ, User::getId)
                .eq(Score::getExamId, 1)
                .select(MAX(Score::getScore), "name")
                .select(Score::getUserId)
                .one();
        System.out.println(score);
    }
}
