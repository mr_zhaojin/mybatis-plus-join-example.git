package com.mybatis.plus.example.service;

import com.mybatis.plus.example.entity.Score;
import com.mybatis.plus.join.service.IBaseService;

public interface IScoreService extends IBaseService<Score> {
}
