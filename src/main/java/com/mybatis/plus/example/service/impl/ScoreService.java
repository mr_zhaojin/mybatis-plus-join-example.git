package com.mybatis.plus.example.service.impl;

import com.mybatis.plus.example.entity.Score;
import com.mybatis.plus.join.service.impl.BaseService;
import com.mybatis.plus.example.mapper.ScoreMapper;
import com.mybatis.plus.example.service.IScoreService;
import org.springframework.stereotype.Service;

@Service
public class ScoreService extends BaseService<ScoreMapper, Score> implements IScoreService {
}
